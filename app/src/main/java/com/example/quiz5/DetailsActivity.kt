package com.example.quiz5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.activity_details.*

import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.util.*


class DetailsActivity : AppCompatActivity() {

    var songlist= mutableListOf<MyModel2>()
   lateinit  var adapter:Adapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        init()
    }

    private  fun  init(){

        var untent=intent.extras
        info.text=untent?.getString("info")

        var groupname=untent?.getString("name")

        ApiRequest.getRequest("5ec3ca1c300000e5b039c407",object : CallbackApi{
            override fun onResponse(value: String?) {
                var jsonobj=JSONObject(value)
                var data=jsonobj.getJSONArray("data")


                (0 until data.length()).forEach(){
                    i->
                    var model= Gson().fromJson<MyModel2>(data[i].toString(),MyModel2::class.java)



                    if(model.band==groupname){
                        songlist.add(model)

                    }

                }


                adapter= Adapter(songlist[0].songs)
                recycle.layoutManager= LinearLayoutManager(this@DetailsActivity)
                recycle.adapter=adapter


             }

            override fun onFailure(value: String?) {

             }
        },this)

    }
}
