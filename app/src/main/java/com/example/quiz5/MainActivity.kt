package com.example.quiz5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonArray
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    var mylist= mutableListOf<MyModel>()
    lateinit var adapter:Adapter2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        ApiRequest.getRequest("5ec3ab0f300000850039c29e",object:CallbackApi{
            override fun onResponse(value: String?) {
                var jsonarray=JSONArray(value)
                (0 until jsonarray.length()).forEach(){
                    i->

                    var model=Gson().fromJson<MyModel>(jsonarray[i].toString(),MyModel::class.java)
                    mylist.add(model)

                }

                var ranndomband=(0 until  mylist.size).random()


              //  Glide.with(this@MainActivity).load("http://static.thetoptens.com/img/lists/335.jpg").into(img)

                adapter= Adapter2(mylist,object : getposition{
                    override fun getpos(posi: Int) {
                        var intent=Intent(this@MainActivity,DetailsActivity::class.java)
                        intent.putExtra("name",mylist[posi].name)
                        intent.putExtra("info",mylist[posi].info)
                        startActivity(intent)
                     }
                })

                recycle1.layoutManager= LinearLayoutManager(this@MainActivity)
                recycle1.adapter=adapter




            }

            override fun onFailure(value: String?) {
                d("ffff",value)
             }
        },this)
    }

}
