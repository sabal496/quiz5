package com.example.quiz5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.list_view.view.*

class Adapter(val mylist:MutableList<MyModel2.items>): RecyclerView.Adapter<Adapter.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.list_view,parent,false))
    }

    override fun getItemCount(): Int {
        return mylist.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
    }

    inner class holder(itemView: View): RecyclerView.ViewHolder(itemView){
        private  lateinit var model:MyModel2.items

        fun onbind(){
            model=mylist[adapterPosition]

            itemView.songlistt.text=model.title

            itemView.setOnClickListener(){
                Toast.makeText(itemView.context,model.title,Toast.LENGTH_SHORT).show()
            }


        }


    }
}