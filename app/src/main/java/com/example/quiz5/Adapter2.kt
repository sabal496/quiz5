package com.example.quiz5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_main.view.*
import kotlinx.android.synthetic.main.list_view.view.*

class Adapter2(val mylist:MutableList<MyModel>,private val pos:getposition): RecyclerView.Adapter<Adapter2.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        return holder(LayoutInflater.from(parent.context).inflate(R.layout.layout_main,parent,false))
    }

    override fun getItemCount(): Int {
        return mylist.size
    }



    inner class holder(itemView: View): RecyclerView.ViewHolder(itemView){
        private  lateinit var model:MyModel

        fun onbind(){
            model=mylist[adapterPosition]

            itemView.nameee.text=model.name

            Glide.with(itemView.context).load(model.img_url).into(itemView.imagee)

            itemView.details.setOnClickListener(){
                pos.getpos(adapterPosition)
            }



        }


    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.onbind()
     }


}