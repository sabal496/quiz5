package com.example.quiz5

interface CallbackApi {
    fun onResponse(value:String?)
    fun onFailure(value:String?)
}